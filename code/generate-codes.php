<?php

class GenerateCodes 
{
    private function generateRandomString($length = 10) {
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    private function generateRandomNumber($length = 10) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    private function generateRandomCode(){
        $str = $this->generateRandomString(3);
        $str .= $this->generateRandomNumber(3);
        return $str;
    }
    
    public function generateRandomCodes() {
        $codes = file_get_contents('codes.txt');
        $usedCodes = file_get_contents('used.txt');
        if($codes == $usedCodes or file_exists('codes.txt') == false) { 
            $arr = [];
            While(1){
            $str = $this->generateRandomCode();
            $arr[$str] = $str;
            if(count($arr)>=500) break;
            }

            $this->writeToFile($arr);
        } else {
            echo "SOME CODES ARE NOT USED!!!";
        }
    }
    
    private function writeToFile($data){
        $file = 'codes.txt';
        $data = json_encode($data);
        file_put_contents($file, $data);
    }

}
$randomCodes = new GenerateCodes;
$randomCodes->generateRandomCodes();

